var uuid = require('node-uuid');
// var movies = [{
// 	title: 'The Godfather',
// 	rating: '9,3',
// 	genre: 'crime|drama',
// storyline: 'blabla'
// }];

var movies = [];

//GET method
exports.getAllMovies = function getAllMovies(req, res) {
	// return res.send(movies);
	res.send(movies);
};

//POST method
exports.addNewMovie = function addNewMovie(req, res) {
	var movieId = 'id' + '_' + uuid.v4();
	var obj = {
		id: movieId,
		title: req.body.title,
		rating: req.body.rating,
		genre: req.body.genre,
		release: req.body.release,
		storyline: req.body.storyline
	};

	movies.push(obj);
	return res.send('The title is ' + req.body.title);	
};

//GET by ID method
exports.getMovieById = function getMovieById(req, res) {
	movies.forEach(function(item) {
		if(item.id === req.params.id){
			res.send(item);
		}
	});
	return res.send(404).send('Not found');
};

//PUT method
exports.editMovie = function editMovie(req, res) {
	movies.forEach(function(item) {
		if(item.id === req.params.id){
			item.title = req.body.title;
			item.rating = req.body.rating;
			item.genre = req.body.genre;
			item.release = req.body.release;
			item.storyline = req.body.storyline;
			
			return res.send('Welcome ');
		}
	});
	res.status(404).send('Not found');
};

//DELETE method
exports.deleteMovie = function deleteMovie(req, res) {
	var id = req.params.id;
	if(!id) {
		res.status(409).res.send('is mandatory');
		return;
	}

	movies.forEach(function(item, index) {
		if(item.id === id) {
			movies.splice(index, 1);
			return;
		}
	});

	return res.send('Movie was deleted');
};
