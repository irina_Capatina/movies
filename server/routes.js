exports.routes =  function routes (app) {
	var movies  = require('./components/movies');
	
	//fetching all movies
	//return arr of movies

	//GET method
	app.get('/movies', movies.getAllMovies);
	//GET by id
	app.get('/movies/:id', movies.getMovieById);
	// //POST method
	app.post('/movies', movies.addNewMovie);
	// //PUT method
	app.put('/movies/:id', movies.editMovie);
	// // DELETE method
	app.delete('/movies/:id', movies.deleteMovie);

};
