(function () {
	'use strict';

	angular
		.module('movies')
		.factory('ConfigService', [ConfigService]);

	function ConfigService () {
		var Config = {};

		Config.apiUrl = 'http://our-domain-name.com/';

		Config.data = 'some data';

		Config.curretMovie = {};

		Config.validation = function validation () {

		};

		return Config;
	}
}());