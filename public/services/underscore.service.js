(function () {
	'use strict';

	angular
		.module('movies')
		.factory('_', ['$window', UnderscoreService]);

	function UnderscoreService ($window) {
		return $window._;
	}
	
}());