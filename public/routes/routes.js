(function () {
	'use strict';

	angular
		.module('movies')
		.config(routes);

	function routes ($stateProvider, $urlRouterProvider) {
		$stateProvider 
			.state({
				name: 'app',
				url: '/app',
				controller: 'AppController',
				controllerAs: 'app',
				templateUrl: 'components/app/templates/app.template.html'
			})
			.state({
				name: 'app.about',
				url: '/about',
				controller: 'AboutController',
				controllerAs: 'about',
				templateUrl: 'components/about/templates/about.template.html'
			})
			.state({
				name: 'app.movies',
				url: '/movies',
				controller: 'MoviesController',
				controllerAs: 'movies',
				templateUrl: 'components/movies/templates/movies.template.html'
			})
			.state({
				name: 'app.movie',
				url: '/movie',
				controller: 'MovieController',
				controllerAs: 'movie',
				templateUrl: 'components/movies/templates/movie.template.html'
			})
			.state({
				name: 'app.movie.details',
				url: '/details/:id',
				templateUrl: 'components/movies/templates/details.template.html'
			})
			.state({
				name: 'app.movie.edit',
				url: '/edit/:id',
				templateUrl: 'components/movies/templates/form.template.html'
			})
			.state({
				name: 'app.movie.add',
				url: '/add',
				templateUrl: 'components/movie/templates/form.template.html'
			});

			//Load default URL
		$urlRouterProvider.otherwise('app');
	}

}());