angular
	.module('movies')
	.controller('MainController', [MainController]);

function MainController() {
	var self = this;
	//for orderBy
	self.sortType = 'title';
	self.sortType = 'rating';
	self.sortType = 'storyline';

	self.sortReverse = false; 
	self.searchItem = '';

	self.currentPage = 0;
	self.itemsPerPage = 3;
	
	self.movies = [];

	self.mainPage = function () {
		return self.currentPage == 0;
	};

	self.lastPage = function () {
		var lastPageNr = Math.ceil(self.movies.length / self.itemsPerPage - 1);
		return self.currentPage == lastPageNr;
	};

	self.nrOfPages = function () {
		return Math.ceil(self.movies.length / self.itemsPerPage);
	};

	self.startingItem = function () {
		return self.currentPage * self.itemsPerPage;
	};

	self.pageBack = function () {
		self.currentPage = self.currentPage - 1;
	};

	self.pageForward = function () {
		self.currentPage = self.currentPage + 1;
	};
}

	// search by rating > 8,9
angular
	.module('movies')
	.filter('RatingFilter', ['$sce', RatingFilter]);

function RatingFilter ($sce) {
	return function (value) {
		if(value > '8,9') {
			return $sce.trustAsHtml('<strong><i>' + value + '</strong></i>');
		} else {
			return $sce.trustAsHtml(value);
		}
	};
}

angular
	.module('movies')
	.filter('StartFrom', [StartFrom]);

function StartFrom () {
	return function (input, start) {
		return input.slice(start);
	};
}

angular
	.module('movies')
	.filter('FindTxt', ['$sce', FindTxt]);

function FindTxt($sce) {
	return function(text, phrase) {
		if (phrase) {
			text = text.replace(new RegExp('('+phrase+')', 'gi'),
		'<span class="highlighted">$1</span>');
		}
		return $sce.trustAsHtml(text);
	};

}
$(document).ready(function(){
    $('.parallax').parallax();
});