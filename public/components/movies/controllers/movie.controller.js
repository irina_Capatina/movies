(function () {
	'use strict';

	angular
		.module('movies')
		.controller('MovieController', ['$state', '$stateParams', '$http',  MovieController]);


	function MovieController($state, $stateParams, $http) {

		var self = this;
		//self.movie = {};

		console.log('stateparams: ', $stateParams, $state.params);

		//show movies by id
		self.getById = function getById () {
			$http.get('/movies/' + $state.params.id) 
				.then(function success(res) {
					self.movies = res.data[0];
					console.log(res);
				}, function error(err) {
					console.log(err);
				});
		};

		self.getById();

		self.addNewMovie = function addNewMovie () {
			$http.post('/movies', self.movie)
			.then(function success(res) {
				console.log(res.data);
				self.arr = res.data;
				console.log('Movie added ', self.arr);
				$state.go('app.movies');
			}, function error(err) {
				console.log(err);
			});
		};

		//edit saved movie
		self.editMovie = function editMovie () {
			$http.put('/movies/' + $state.params.id, self.movie)
			.then(function success(res) {
				console.log(res.data);
				$state.go('app.movies');
			}, function error(err) { 
				console.log(err);
			});
		};

			self.validOnChanges = function validOnChanges () {
			console.log($state.current.title);
			if( $state.current.title === 'app.movie.edit') {

				self.editMovie();

			} else {
				console.log($state.current.title);
				self.addNewMovie();

			}
		}

	}
}());