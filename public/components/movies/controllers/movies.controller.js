(function() {
	'use strict';

	angular
		.module('movies')
		.controller('MoviesController', ['$state', '$http', MoviesController ]);

	function MoviesController($state, $http) {
		var self = this;

		// console.log('MoviesController');

		self.getMovie = function getMovie(id) {
			$state.go('app.movie.details', {id: id});
		};

		self.showAllMovies = function showAllMovies() {

			$http.get('/movies')
				.then(function success(res) {
					self.movies = res.data;
					console.log(res);
				}, function error(err) {
					console.log(err);
				});
		};

		self.showAllMovies();

		self.editMovie = function editMovie(id) {
			$state.go('app.movie.edit', {id: id}, {reload: true});
		};

		self.deleteMovie = function deleteMovie(id, index) {
			console.log('The movie was deleted ', id);

			$http.delete('/movies/' + id)
			.then(function success(res) {
				self.movies.splice(index, 1);
				console.log(res);
				$state.go('app.movies', {id: id},  {reload: true});
			}, function error(err) {
				console.log(err);
			});
		};

		function sortData() {
			return [];
		}
	}

}());


